const actionsTypes = {
  SET_SELECTED_FORM: "SET_SELECTED_FORM",
  SET_FIELD_TOEDIT: "SET_FIELD_TOEDIT",
  PUSH_FIELD: "PUSH_FIELD",
  PUSH_FIELD_ALL: "PUSH_FIELD_ALL",
};
export default actionsTypes;

export function setSelectedForm(data: any) {
  return {
    type: actionsTypes.SET_SELECTED_FORM,
    payload: data,
  };
}

export function setFieldToEdit(data: any) {
  return {
    type: actionsTypes.SET_FIELD_TOEDIT,
    payload: data,
  };
}
export function pushFields(data: any) {
  return {
    type: actionsTypes.PUSH_FIELD,
    payload: data,
  };
}

export function pushFieldsAll(data: any) {
  return {
    type: actionsTypes.PUSH_FIELD_ALL,
    payload: data,
  };
}
