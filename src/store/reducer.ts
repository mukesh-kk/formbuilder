import actionsTypes from "./actions";

export function formReducer(
  state: any = {
    selectedForm: {},
    allForms: [],
    fieldsArray: [],
    fieldToEdit: {},
  },
  action: any
) {
  switch (action.type) {
    case actionsTypes.SET_SELECTED_FORM: {
      return {
        ...state,
        selectedForm: action.payload,
      };
    }
    case actionsTypes.SET_FIELD_TOEDIT: {
      return { ...state, fieldToEdit: action.payload };
    }
    case actionsTypes.PUSH_FIELD: {
      const res = state.fieldsArray.filter(
        (each) => each?.id != action.payload[0]?.id
      );
      action.payload[0] && res.push(action.payload[0]);
      return {
        ...state,
        fieldsArray: [...res],
      };
    }
    case actionsTypes.PUSH_FIELD_ALL: {
      return { ...state, fieldsArray: action.payload };
    }
    default:
      return {
        ...state,
      };
  }
}
