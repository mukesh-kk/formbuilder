import React from "react";
import "./EachFiledScreen.css";
import EachFieldSetting from "../eachFieldSetting/EachFieldSetting";
import { useSelector } from "react-redux";
import { RootState } from "../../store/store";

export default function EachFiledScreen() {
  const fieldToEdit = useSelector(
    (state: RootState) => state.forms.fieldToEdit
  );
  return (
    <>
      <div className="header">
        <p>Edit/Add Fields </p>
      </div>
      <div className="each-filed-screnn">
        <EachFieldSetting data={fieldToEdit} />
      </div>
    </>
  );
}
