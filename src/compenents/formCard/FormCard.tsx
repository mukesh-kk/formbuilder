import React from "react";
import { navigateToUrl } from "single-spa";
import "./FormCard.css";
import { useDispatch } from "react-redux";
import {
  pushFields,
  pushFieldsAll,
  setSelectedForm,
} from "../../store/actions";
import { useNavigate } from "react-router-dom";
function getDate(str: string) {
  return (new Date(str) + "").substring(0, 16);
}
export default function FormCard({ data }) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  return (
    <div className="each-form-card">
      <p>{data.title}</p>
      <p> Created On {getDate(data.created_at)}</p>

      <button
        onClick={() => {
          dispatch(setSelectedForm(data));
          dispatch(pushFieldsAll(data.fields));
          navigate("/form");
        }}
      >
        View/Edit
      </button>
    </div>
  );
}
