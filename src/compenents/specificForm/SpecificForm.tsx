import React, { useEffect } from "react";
import FormCard from "../formCard/FormCard";
import FieldsCard from "../fieldsCard/FieldsCard";
import "./SpecificForm.css";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store/store";
import { setFieldToEdit, setSelectedForm } from "../../store/actions";
import axios from "axios";
export default function SpecificForm() {
  const selectedCard = useSelector(
    (store: RootState) => store.forms.selectedForm
  );
  const fields = useSelector((store: RootState) => store.forms.fieldsArray);

  const dispatch = useDispatch();
  useEffect(() => {
    if (!selectedCard.title) {
      axios
        .post("http://localhost:3000/forms/", {
          title: "Form" + Math.round(100000 * Math.random()),
          fields: fields,
        })
        .then((res) => {
          dispatch(setSelectedForm(res.data));
        });
    }
    dispatch(setFieldToEdit({}));
  }, []);

  useEffect(() => {
    axios.put("http://localhost:3000/forms/" + selectedCard._id, {
      ...selectedCard,
      fields: fields,
    });
  }, [fields]);
  return (
    <>
      {" "}
      <div className="header">
        <p>{selectedCard.title} </p>
      </div>
      <div className="specficiform-wrpaper">
        <FieldsCard />
      </div>
    </>
  );
}
