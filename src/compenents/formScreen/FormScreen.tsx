import React, { useEffect, useState } from "react";
import "./FormScreen.css";
import FormCard from "../formCard/FormCard";
import FieldsCard from "../fieldsCard/FieldsCard";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const data = [{ createdAt: new Date(), title: "Form 1" }];
export default function FormScreen() {
  const [allForm, setAllForm] = useState([]);
  useEffect(() => {
    axios.get("http://localhost:3000/forms").then((res) => {
      console.log(res);
      setAllForm(res.data);
    });
  }, []);
  const navigate = useNavigate();
  return (
    <div>
      <div className="header">
        <p>All Forms</p>
        <button onClick={() => navigate("/form")}>New Form</button>
      </div>

      <div className="wrapper-all-form">
        {allForm.map((each: any) => {
          return (
            <div key={each.title} className="each-card-wrpaper">
              <FormCard data={each} />
            </div>
          );
        })}
      </div>
      <div>
        <FieldsCard />
      </div>
    </div>
  );
}
