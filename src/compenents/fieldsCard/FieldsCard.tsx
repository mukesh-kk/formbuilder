import React, { useEffect, useState } from "react";
import "./FieldsCard.css";
import edit from "../../../assets/edit.svg";
import EachFieldSetting from "../eachFieldSetting/EachFieldSetting";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store/store";
import { useNavigate } from "react-router-dom";
import {
  pushFields,
  setFieldToEdit,
  setSelectedForm,
} from "../../store/actions";
import {
  Button,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  TextareaAutosize,
} from "@mui/material";

export default function FieldsCard() {
  const [selectedField, setSelectedField] = useState(null);
  const dispatch = useDispatch();
  const fields = useSelector((store: RootState) => store.forms.fieldsArray);

  const navigate = useNavigate();
  // useEffect(() => {
  //   dispatch(pushFields([]));
  // }, []);

  return (
    <div className="wrapper-formsec">
      <div className="view-section-left">
        <p style={{ marginBottom: "1em" }}>Fields</p>
        {fields?.map((each: any, i) => {
          return (
            <div
              style={{
                display: "flex",
                width: "150px",
                justifyContent: "space-between",
                marginTop: "7px",
              }}
            >
              {i + 1} {"." + each?.name}{" "}
              <img
                className=""
                onClick={() => {
                  dispatch(setFieldToEdit(each));
                  navigate("/edit");
                }}
                src={edit}
                alt="e"
              />
            </div>
          );
        })}

        <div>
          {" "}
          <Button
            size="small"
            variant="contained"
            onClick={() => navigate("/edit")}
            style={{ marginTop: "2em" }}
          >
            Add
          </Button>
        </div>
      </div>
      <p>Form Viewer</p>
      <div className="view-section-right">
        <div className="formview-area">
          {fields.map((eachField: any) => {
            if (eachField.type == "TEXT") {
              return (
                <TextField
                  required={eachField.required}
                  id="filled-required"
                  label={eachField.label}
                  name={eachField.name}
                  variant="filled"
                />
              );
            }
            if (eachField.type == "TEXT_AREA") {
              return (
                <TextareaAutosize
                  aria-label="minimum height"
                  minRows={5}
                  placeholder={eachField.label}
                  style={{ width: 200 }}
                  name={eachField.name}
                />
              );
            }
            if (eachField.type == "FILE") {
              return (
                <>
                  <input type="file" />
                </>
              );
            }
            if (eachField.type == "SELECT") {
              return (
                <>
                  <InputLabel id="demo-simple-select-label">Type</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={""}
                    label="Type"
                    style={{ width: "200px" }}
                  >
                    {eachField.config.options.map((eh) => {
                      <MenuItem key={eh.label} value={eh.value}>
                        {eh.label}
                      </MenuItem>;
                    })}
                  </Select>
                </>
              );
            }
          })}

          <Button>Submit</Button>
        </div>
      </div>
    </div>
  );
}
