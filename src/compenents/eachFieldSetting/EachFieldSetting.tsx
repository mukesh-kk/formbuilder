import React, { useEffect, useState } from "react";
import "./EachFieldSetting.css";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import edit from "../../../assets/edit.svg";
import {
  Button,
  FormControl,
  FormHelperText,
  Input,
  InputLabel,
  MenuItem,
  Select,
  Switch,
} from "@mui/material";
import { z } from "zod";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
const FieldType = z.enum(["TEXT", "SELECT", "TEXT_AREA", "FILE"]);
import { v4 as uuidv4 } from "uuid";
import { pushFields } from "../../store/actions";
import { RootState } from "../../store/store";
export const baseFormField = z.object({
  id: z.string(),
  name: z.string().min(2),
  label: z.string().min(2),
  required: z.boolean(),
  type: FieldType,
  config: z
    .object({
      options: z
        .array(
          z.object({
            label: z.string().optional(),
            value: z.string().optional(),
          })
        )
        .optional(),
    })
    .optional(),
});

export default function EachFieldSetting({ data }) {
  const [type, setType] = useState("TEXT");
  const [options, setOptions] = useState([]);
  const [required, setRequired] = useState(false);
  const [nameLabel, setNameLabel] = useState({ name: "", label: "" });
  const [eachOptions, setEachOptions] = useState({ value: "", label: "" });
  const [isValid, setIsValid] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const fieldToEdit = useSelector(
    (state: RootState) => state.forms.fieldToEdit
  );

  useEffect(() => {
    if (baseFormField.safeParse(fieldToEdit).success) {
      setType(fieldToEdit.type);
      setRequired(fieldToEdit.required);
      setOptions(fieldToEdit.config.options);
      setNameLabel({
        name: fieldToEdit.name,
        label: fieldToEdit.label,
      });
    }
  }, [fieldToEdit]);
  useEffect(() => {
    const obj = {
      id: uuidv4(),
      name: nameLabel.name,
      label: nameLabel.label,
      required: required,
      type: type,
      config: { options: options },
    };

    const res = baseFormField.safeParse(obj);

    if (res.success) {
      setIsValid(true);
    } else {
      setIsValid(false);
    }
  }, [type, options, required, nameLabel]);
  return (
    <div>
      <p>Each Field Setting</p>
      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "30ch", minWidth: 120 },
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          id="name"
          label="Name"
          variant="standard"
          value={nameLabel.name}
          onChange={(e) => {
            setNameLabel({ ...nameLabel, name: e.target.value });
          }}
        />
        <TextField
          id="label"
          label="Label"
          variant="standard"
          value={nameLabel.label}
          onChange={(e) => {
            setNameLabel({ ...nameLabel, label: e.target.value });
          }}
        />
        {/* Required************ */}
        <div>
          <p>Required:</p>
          <Switch value={required} onChange={(e, boll) => setRequired(boll)} />
        </div>
        {/* //type ***************/}
        <div>
          <InputLabel id="demo-simple-select-label">Type</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={type}
            label="Type"
            onChange={(event) => {
              setType(event.target.value);
            }}
            style={{ width: "200px" }}
          >
            <MenuItem value={"TEXT"}>Text</MenuItem>
            <MenuItem value={"TEXT_AREA"}>Text Area</MenuItem>
            <MenuItem value={"SELECT"}>Select</MenuItem>
            <MenuItem value={"FILE"}>File</MenuItem>
          </Select>
        </div>
        {/* select */}
        {type == "SELECT" ? (
          <>
            <div>
              <p>Enter Options for Select</p>
              {options.map((each, i) => {
                return (
                  <div className="each-fieldnv" key={i}>
                    <p>{` ${i + 1} . ${each.label} ->${each.value}`}</p>{" "}
                    <img
                      src={edit}
                      onClick={() => {
                        setEachOptions(each);
                        setOptions(
                          options.filter((ea) => ea.label !== each.label)
                        );
                      }}
                      alt=""
                    />
                  </div>
                );
              })}
              <div
                style={{
                  display: "flex",
                  margin: "1em",
                  justifyContent: "space-around",
                }}
              >
                <FormControl>
                  <InputLabel htmlFor="my-input">Label</InputLabel>
                  <Input
                    id="my-input"
                    aria-describedby="my-helper-text"
                    onChange={(e) =>
                      setEachOptions({ ...eachOptions, label: e.target.value })
                    }
                    value={eachOptions.label}
                  />
                  {/* <FormHelperText id="my-helper-text">
                  We'll never share your email.
                </FormHelperText> */}
                </FormControl>
                <FormControl>
                  <InputLabel htmlFor="my-input-2">Value</InputLabel>
                  <Input
                    id="my-input-2"
                    aria-describedby="my-helper-text-2"
                    value={eachOptions.value}
                    onChange={(e) =>
                      setEachOptions({ ...eachOptions, value: e.target.value })
                    }
                  />{" "}
                </FormControl>
                <Button
                  variant="outlined"
                  size="small"
                  onClick={() => {
                    setOptions([...options, eachOptions]),
                      setEachOptions({ value: "", label: "" });
                  }}
                >
                  Add
                </Button>
              </div>
            </div>
          </>
        ) : null}
      </Box>
      <div style={{ marginTop: "2em" }}>
        <Button
          variant="contained"
          size="small"
          disabled={!isValid}
          onClick={() => {
            dispatch(
              pushFields([
                {
                  id: fieldToEdit.id ? fieldToEdit.id : uuidv4(),
                  name: nameLabel.name,
                  label: nameLabel.label,
                  required: required,
                  type: type,
                  config: { options: options },
                },
              ])
            );
            navigate("/form");
          }}
        >
          Submit
        </Button>
      </div>
    </div>
  );
}
