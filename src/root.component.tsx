import FormScreen from "./compenents/formScreen/FormScreen";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import SpecificForm from "./compenents/specificForm/SpecificForm";
import EachFiledScreen from "./compenents/eachFieldScreen.tsx/EachFiledScreen";
import { Provider } from "react-redux";
import store from "./store/store";
const router = createBrowserRouter([
  {
    path: "/",
    element: <FormScreen></FormScreen>,
  },
  {
    path: "/form",
    element: <SpecificForm />,
  },
  {
    path: "/edit",
    element: <EachFiledScreen />,
  },
]);
export default function Root(props) {
  return (
    <>
      <Provider store={store}>
        <RouterProvider router={router} />
      </Provider>
    </>
  );
}
